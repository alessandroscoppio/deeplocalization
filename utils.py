import re

import numpy as np
import open3d as o3d
from pyquaternion import Quaternion

import euler_utils


# from evo.tools import file_interface
# from evo.tools import plot
#
#
# def evo_plot_imuvsgt_trajectories(imu_tum_filename, gt_tum_filename):
#     print(imu_tum_filename)
#     print(gt_tum_filename)
#     imu_traj = file_interface.read_tum_trajectory_file(imu_tum_filename)
#     gt_traj = file_interface.read_tum_trajectory_file(gt_tum_filename)
#
#     fig = plt.figure()
#     traj_by_label = {
#         "ground truth": gt_traj,
#         "IMU": imu_traj
#     }
#     plot.trajectories(fig, traj_by_label, plot.PlotMode.xyz)
#     plot.draw_coordinate_axes(ax=fig.axes[0], traj=imu_traj, plot_mode=plot.PlotMode.xyz)
#     plt.show()


def natural_key(string_):
    """See https://stackoverflow.com/questions/2545532/python-analog-of-phps-natsort-function-sort-a-list-using-a-natural-order-alg"""
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]


def subtract_pose(pose1, pose2):
    """
    Given pose1 and pose2 in form of p = [x, y, z, R, P, Y] return the difference between them.
    """
    xyz1 = pose1[0:3]
    w1, x1, y1, z1 = euler_utils.euler2quat(z=pose1[5], y=pose1[4], x=pose1[3])
    quat1 = Quaternion(w1, x1, y1, z1)
    xyz2 = pose2[0:3]
    w2, x2, y2, z2 = euler_utils.euler2quat(z=pose2[5], y=pose2[4], x=pose2[3])
    quat2 = Quaternion(w2, x2, y2, z2)
    xyz = xyz1 - xyz2
    rpy = quat1 * quat2.inverse
    rpy = euler_utils.quat2mat(rpy)
    y, p, r = euler_utils.mat2euler(rpy)
    return np.hstack((xyz, np.hstack((r, p, y))))


def subtract_quat_pose(pose1, pose2):
    xyz1 = pose1[0:3]
    w1, x1, y1, z1 = pose1[3:]
    quat1 = Quaternion(w1, x1, y1, z1)
    xyz2 = pose2[0:3]
    w2, x2, y2, z2 = pose2[3:]
    quat2 = Quaternion(w2, x2, y2, z2)
    xyz = xyz1 - xyz2
    # from: https://www.researchgate.net/post/How_do_I_calculate_the_smallest_angle_between_two_quaternions
    # the angle between 2 quats is 2*np.arcsin(np.linalg.norm(quat.vector))
    # quat = quat1 * quat2.inverse
    quat = quat1 / quat2
    return np.hstack((xyz, quat.elements))


def get_tfmat_from_pose(pose):
    """ Assemble a 4x4 tf matrix from a pose: [x, y, z, qw, qx, qy, qz]"""
    # assemble transformation matrix of pose difference
    rot_mat = euler_utils.quat2mat(pose[3:])
    # rot_mat is returned transposed
    translation = pose[0:3]
    tf_mat = np.vstack((np.hstack((rot_mat, translation.reshape(3, 1))), [0, 0, 0, 1]))
    return tf_mat


def invert_tfmat(tf_mat):
    """ Given a homogeneous transformation matrix, return its inverse defined as:
    tf^-1 = [[R^-1  -R^-1*t]
            [   0      1   ]
    """
    rot_mat = tf_mat[0:3, 0:3]
    t = tf_mat[:3, 3]
    # for rot matrix inverse = transpose, which is more efficient
    inv_rot_mat = rot_mat.T
    new_t = np.matmul(-inv_rot_mat, t.reshape(3, 1))
    inv_tf_mat = np.vstack((np.hstack((inv_rot_mat, new_t)), [0, 0, 0, 1]))
    return inv_tf_mat


def add_pose(pose1, pose2):
    """
    Given pose1 and pose2 in form of p = [x, y, z, R, P, Y] return the sum of them.
    """
    xyz1 = pose1[0:3]
    w1, x1, y1, z1 = euler_utils.euler2quat(z=pose1[5], y=pose1[4], x=pose1[3])
    quat1 = Quaternion(w1, x1, y1, z1)
    xyz2 = pose2[0:3]
    w2, x2, y2, z2 = euler_utils.euler2quat(z=pose2[5], y=pose2[4], x=pose2[3])
    quat2 = Quaternion(w2, x2, y2, z2)
    xyz = xyz1 + xyz2
    rpy = quat2 * quat1
    rpy = euler_utils.quat2mat(rpy)
    y, p, r = euler_utils.mat2euler(rpy)
    return np.hstack((xyz, np.hstack((r, p, y))))


def make_open3d_point_cloud(xyz, color=None):
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(xyz)
    if color is not None:
        pcd.colors = o3d.utility.Vector3dVector(color)
    return pcd


def rot_x(x):
    out = np.zeros((3, 3))
    c = np.cos(x)
    s = np.sin(x)
    out[0, 0] = 1
    out[1, 1] = c
    out[1, 2] = -s
    out[2, 1] = s
    out[2, 2] = c
    return out


def rot_y(x):
    out = np.zeros((3, 3))
    c = np.cos(x)
    s = np.sin(x)
    out[0, 0] = c
    out[0, 2] = s
    out[1, 1] = 1
    out[2, 0] = -s
    out[2, 2] = c
    return out


def rot_z(x):
    out = np.zeros((3, 3))
    c = np.cos(x)
    s = np.sin(x)
    out[0, 0] = c
    out[0, 1] = -s
    out[1, 0] = s
    out[1, 1] = c
    out[2, 2] = 1
    return out
