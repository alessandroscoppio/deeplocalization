# This import registers the 3D projection, but is otherwise unused.
import math

from mpl_toolkits import mplot3d
import time

# import pandas as pd
import h5py
import matplotlib.pyplot as plt
from tqdm import tqdm
from utils import *


class DataPreprocessor:

    def __init__(self, dataset_directory, filename, config_file):
        self.dataset_directory = dataset_directory
        self.filename = filename
        self.dataset_name = filename[:-4]
        self.steps = []
        self.N = None
        self.pcds = []
        self.imgs = []
        self.imu_batches = []
        self.gt_batches = []

        # load dataset from numpy file
        self.load_npy_file(skip_steps=0)

        # # pcd messages are always in first position
        # self.pcds = np.asarray(self.steps[:, 0])
        # if config_file is None:
        #     self.imgs = np.asarray(self.steps[:, 1])
        #     self.imus = np.asarray(self.steps[:, 2])
        #     self.gts = self.steps[:, 3]
        # else:
        #
        #     self.load_config_file(config_file)
        #

    def load_npy_file(self, skip_steps=0):
        """ Load dataset from .npy file """
        start = time.time()
        dataset = np.load(self.dataset_directory + self.filename, allow_pickle=True, encoding='bytes')
        steps = dataset.transpose()
        # first steps are 0s for about 5 seconds
        empty_steps = skip_steps  # define here how many
        self.steps = steps[empty_steps:]  # and remove them
        self.N = len(self.steps)
        self.pcds = self.steps[:, 0]
        self.imgs = self.steps[:, 1]
        self.imu_batches = self.steps[:, 2]
        self.gt_batches = self.steps[:, 3]
        stop = time.time()
        print("Loaded dataset with shape: {}\nTime elapsed: {}s".format(self.N, stop - start))

    def load_h5_file(self):
        """ Load dataset from .h5 file """
        start = time.time()
        self.hf = h5py.File(self.dataset_directory + self.filename, 'r')
        steps = sorted(list(self.hf.keys()), key=natural_key)
        for group in steps:
            grp = self.hf.get(group)
            self.pcds.append(np.asarray(grp.get('pcd')))
            self.imgs.append(np.asarray(grp.get('img')))
            imu_grp = grp.get('imu')
            indexes = imu_grp.keys()
            for idx in indexes:
                diz = imu_grp.get(idx)
                imu = {}
                for elem in list(diz):
                    imu[elem] = diz.get(elem).value
                self.imus.append(imu)

            gt_grp = grp.get('gt')
            gt = {}
            for elem in list(gt_grp):
                gt[elem] = gt_grp.get(elem).value
            self.gts.append(gt)
        self.N = len(steps)
        stop = time.time()
        print("Loaded dataset with shape: {}\nTime elapsed: {}s".format(self.N, stop - start))

    def load_config_file(self, config_file):
        config_file = open(self.dataset_directory + config_file, 'r')
        lines = config_file.readlines()
        print(lines)
        if len(lines) == 2:
            self.imus = self.steps[:, 1]
        elif len(lines) == 3:
            self.imus = self.steps[:, 1]
            # TODO: when not all topics are available there is ambiguity on which are present
            # Solved with info file. Load and read to get available topics
            self.gts = self.steps[:, 2]
        elif len(lines) == 4:
            self.imgs = self.steps[:, 1]
            self.imus = self.steps[:, 2]
            self.gts = self.steps[:, 3]

    def print_last_step(self):
        print("Printing last step...")
        last_step = self.steps[-1]
        pcd = last_step[0]
        img = last_step[1]
        imu = last_step[2]
        gt = last_step[3]

        pcd = make_open3d_point_cloud(pcd)
        plt.imshow(img)
        plt.show()
        o3d.visualization.draw_geometries([pcd], window_name="DeepLoc")
        print(imu)
        print(gt)

    def plot_trajectory(self, poses=None):
        if poses is None:
            xyz = self.get_xyz_from_gt(step="one_per_step")
            xs = xyz[:, 0]
            ys = xyz[:, 1]
            zs = xyz[:, 2]
            rpy = self.get_rpy_from_gt(step="one_per_step")
            thetas = rpy[:, 2]
        else:
            xyz = poses[:, 0:3]
            xs = xyz[:, 0]
            ys = xyz[:, 1]
            zs = xyz[:, 2]
            thetas = poses[:, 5]

        fig = plt.figure(figsize=(17, 17))
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        min_x, max_x = np.min(xs), np.max(xs)
        min_y, max_y = np.min(ys), np.max(ys)
        min_z, max_z = np.min(zs), np.max(zs)
        ax.set_xlim([min_x, max_x])
        ax.set_ylim([min_y, max_y])
        ax.set_zlim([min_z, max_z])
        arrow_len = 5 * (max_x - min_x) / len(xs)

        for idx in tqdm(range(len(xs))):
            ax.scatter3D(xs[idx], ys[idx], zs[idx], c='red')
            ax.quiver(xs[idx], ys[idx], zs[idx], np.cos(thetas[idx]), np.sin(thetas[idx]), 0, length=arrow_len,
                      normalize=True)
            plt.pause(0.05)
        plt.show()

        print("Plotted trajectory, total poses: {}".format(self.N))

    def absolute_to_relative_pose(self):
        """
        Convert ground truth pose to be relative to the previous step
        """
        # initialize first pose to zero
        start_pose = np.array([0, 0, 0, 0, 0, 0])
        relative_poses = []
        new_abs_poses = []
        relative_poses.append(start_pose)

        xyz_absolute_poses = self.get_xyz_from_gt()
        rpy_absolute_poses = self.get_rpy_from_gt()
        absolute_poses = np.hstack((xyz_absolute_poses, rpy_absolute_poses))
        new_abs_poses.append(absolute_poses[0])
        # populate poses
        for idx in range(1, len(absolute_poses)):
            relative_pose = subtract_pose(absolute_poses[idx], absolute_poses[idx - 1])
            relative_poses.append(relative_pose)

            new_abs_pose = add_pose(new_abs_poses[idx - 1], relative_pose)
            new_abs_poses.append(new_abs_pose)

        relative_poses = np.array(relative_poses)
        new_abs_poses = np.array(new_abs_poses)

        self.save_as_tum(absolute_poses, filename="absolute_comp.txt")
        self.save_as_tum(relative_poses, filename="relative_comp.txt")
        self.save_as_tum(new_abs_poses, filename="new_abs_comp.txt")

    def get_xyz_from_gt(self, step='all'):
        if step == 'all':
            xyzs = []
            for gt_batch in self.gt_batches:
                [xyzs.append([gt_msg[b'transform'][b'translation'][b'x'], gt_msg[b'transform'][b'translation'][b'y'],
                              gt_msg[b'transform'][b'translation'][b'z']]) for
                 gt_msg in
                 gt_batch]
                # xyzs.append(xyz)
            ret = np.array(xyzs)
        elif step == 'one_per_step':
            last_gts = self.last_gts()
            ret = [[gt_msg[b'transform'][b'translation'][b'x'], gt_msg[b'transform'][b'translation'][b'y'],
                    gt_msg[b'transform'][b'translation'][b'z']] for gt_msg in
                   last_gts]
        elif type(step) == int:
            last_gts = self.last_gts()

            # Transform stamped
            ret = [last_gts[step][b'transform'][b'translation'][b'x'],
                   last_gts[step][b'transform'][b'translation'][b'y'],
                   last_gts[step][b'transform'][b'translation'][b'z']]

            # Pose stamped
            # ret = [last_gts[step][b'pose'][b'position'][b'x'],
            #        last_gts[step][b'pose'][b'position'][b'y'],
            #        last_gts[step][b'pose'][b'position'][b'z']]

        return np.array(ret)

    def get_rpy_from_gt(self, step='all'):
        """
        return rpy as rotation around y, x, and z axis.
        NOTE!: yaw is absolute, but pitch and roll are only relative to the drone == base_link frame!
        """
        if step == 'all':
            rpys = []
            for gt_batch in self.gt_batches:
                # roll and pitch are inverted
                [rpys.append([gt_msg[b'rpy'][b'y'], gt_msg[b'rpy'][b'x'], gt_msg[b'rpy'][b'z']]) for gt_msg in
                 gt_batch]
                # rpys.append(rpy)
            ret = rpys
        elif step == 'one_per_step':
            last_gts = self.last_gts()
            # roll and pitch are inverted TODO: there was a + np.pi at the z, check why and if is needed
            ret = [[gt_msg[b'rpy'][b'y'], gt_msg[b'rpy'][b'x'], gt_msg[b'rpy'][b'z']] for
                   gt_msg in last_gts]
        elif type(step) == int:
            # roll and pitch are inverted
            ret = [[gt_msg[b'rpy'][b'y'], gt_msg[b'rpy'][b'x'], gt_msg[b'rpy'][b'z']] for
                   gt_msg in self.gt_batches[step]]

        return np.array(ret)

    def get_quat_from_gt(self, step='all'):
        """
        return quaternion representing orientation of drone in space, in odom frame.
        returns [w, x, y, z]
        """
        if step == 'all':
            quats = []
            for step in range(self.N):
                y, x, z, w = self.gts[step][b'transform'][b'rotation'][b'y'], self.gts[step][b'transform'][b'rotation'][
                    b'x'], \
                             self.gts[step][b'transform'][b'rotation'][b'z'], \
                             self.gts[step][b'transform'][b'rotation'][b'w']
                quat = np.hstack((w, x, y, z))
                quats.append(quat)
            ret = np.array(quats)
        elif step == 'one_per_step':
            last_gts = self.last_gts()
            wxyz = [[gt_msg[b'transform'][b'rotation'][b'w'], gt_msg[b'transform'][b'rotation'][b'x'],
                     gt_msg[b'transform'][b'rotation'][b'y'], gt_msg[b'transform'][b'rotation'][b'z']] for
                    gt_msg in last_gts]
            ret = np.asarray(wxyz)
        elif type(step) == int:
            last_gts = self.last_gts()
            # Transform stamped
            w = last_gts[step][b'transform'][b'rotation'][b'w']
            x = last_gts[step][b'transform'][b'rotation'][b'x']
            y = last_gts[step][b'transform'][b'rotation'][b'y']
            z = last_gts[step][b'transform'][b'rotation'][b'z']

            # Pose stamped
            # w = last_gts[step][b'pose'][b'orientation'][b'w']
            # x = last_gts[step][b'pose'][b'orientation'][b'x']
            # y = last_gts[step][b'pose'][b'orientation'][b'y']
            # z = last_gts[step][b'pose'][b'orientation'][b'z']
            ret = np.hstack((w, x, y, z))
        return ret

    def get_pose_from_gt(self, step='all'):
        xyz = self.get_xyz_from_gt(step=step)
        # rpy = self.get_rpy_from_gt(step=step)
        quat = self.get_quat_from_gt(step=step)
        pose = np.hstack((xyz, quat))
        return pose

    def get_tf_from_pose_rpy(self, step=-1):
        xyz = self.get_xyz_from_gt(step)
        rpy = self.get_rpy_from_gt(step)
        pose = np.hstack((xyz, rpy))
        tf = euler_utils.pose_vec_to_mat(pose)
        return tf

    def get_lin_acceleration_imu(self):
        """ return linear acceleration from IMU measurements"""
        pass

    def get_angular_velocity_imu(self):
        """return angular velcities from IMU measurements"""
        pass

    def get_quats_from_imu(self, step='all'):
        """
        return numpy array with w, x, y, z (scalar, roll, pitch, yaw in quaternion form)
        for one or all steps
        DIFFERENT FROM TUM that puts w in the end!
        """
        if step == 'all':  # all imus in each step
            quats = []
            for step in range(self.N):
                step_quats = []
                for imu_dict in self.imu_batches[step]:
                    y, x, z, w = imu_dict[b'orientation'][b'y'], imu_dict[b'orientation'][b'x'], \
                                 imu_dict[b'orientation'][b'z'], \
                                 imu_dict[b'orientation'][b'w']
                    quat = np.hstack((w, x, y, z))
                    step_quats.append(quat)
                quats.append(np.asarray(step_quats))
            return np.asarray(quats)

        elif step == 'one_per_step':  # only last imu for each step
            last_imus = self.last_imus()
            quats = []
            for imu_dict in last_imus:
                y, x, z, w = imu_dict[b'orientation'][b'y'], imu_dict[b'orientation'][b'x'], \
                             imu_dict[b'orientation'][b'z'], \
                             imu_dict[b'orientation'][b'w']
                quat = np.hstack((w, x, y, z))
                quats.append(quat)
            return np.asarray(quats)

        elif type(step) == int:  # all imus from one single step
            step_quats = []
            for imu_dict in self.imu_batches[step]:
                y, x, z, w = imu_dict[b'orientation'][b'y'], imu_dict[b'orientation'][b'x'], \
                             imu_dict[b'orientation'][b'z'], \
                             imu_dict[b'orientation'][b'w']
                quat = np.hstack((w, x, y, z))
                step_quats.append(quat)
            return np.asarray(step_quats)

    def get_rpy_from_imu(self, step='all'):
        if step == 'all':
            rpys = []
            quats = self.get_quats_from_imu(step='all')
            for step in quats:
                step_rpys = []
                for quat in step:
                    quat = Quaternion(quat)
                    y, p, r = quat.yaw_pitch_roll
                    rpy = np.hstack((r, p, y))
                    step_rpys.append(rpy)
                rpys.append(np.asarray(step_rpys))
            return np.asarray(rpys)
        elif step == 'one_per_step':  # only last imu for each step
            rpys = []
            quats = self.get_quats_from_imu(step="one_per_step")
            for quat in quats:
                quat = Quaternion(quat)
                y, p, r = quat.yaw_pitch_roll
                rpy = np.hstack((r, p, y))
                rpys.append(rpy)
            return np.asarray(rpys)
        else:
            quats = self.get_quats_from_imu(step)
            step_rpys = []
            for quat in quats:
                quat = Quaternion(quat)
                y, p, r = quat.yaw_pitch_roll
                rpy = np.hstack((r, p, y))
                step_rpys.append(rpy)
            return np.asarray(step_rpys)

    def last_imus(self):
        """ returns only the last IMU reading per each step, in order to plot and save as tum """
        last_imus = [step_imus[-1] for step_imus in self.imu_batches]
        return last_imus

    def last_gts(self):
        """ returns only the last groundtruth reading per each step, in order to plot and save as tum """
        last_gts = [step_gts[-1] for step_gts in self.gt_batches]
        return last_gts

    def plot_all_imus(self):
        # xyz = self.get_xyz_from_gt()
        last_gts = self.last_gts()
        xyz = [np.hstack((gt_msg[b'position'][b'x'], gt_msg[b'position'][b'y'], gt_msg[b'position'][b'z'])) for gt_msg
               in last_gts]
        last_imus_quats = self.get_quats_from_imu(step="one_per_step")
        poses = np.hstack((xyz, last_imus_quats))  # [x y z qw qx qy qz]

        # last_imus_rpy = self.get_rpy_from_imu(step="one_per_step")
        # poses = np.hstack((xyz, last_imus_rpy))  # [x y z roll pitch yaw]
        self.plot_trajectory(poses)
        self.save_as_tum(poses=poses)

    def plot_pcds(self, n=None):
        pcds = []
        if n is None:
            n = self.N

        for idx in range(n):
            pcd = np.array(self.pcds[idx])
            if pcd.shape[0] != 0:
                pcds.append(pcd)

        pcds = np.array(pcds)
        mega_pcd = np.vstack([_ for _ in pcds])
        mega_pcd = make_open3d_point_cloud(mega_pcd)
        vis = o3d.visualization.Visualizer()
        vis.create_window()
        vis.get_render_option().show_coordinate_frame = True
        vis.add_geometry(mega_pcd)
        vis.run()
        vis.destroy_window()
        # o3d.visualization.draw_geometries([mega_pcd], window_name="DeepLoc")

    def save_as_tum(self, poses=None, filename="tum.txt"):
        """
        Save poses as tum format for visualization and conversion using EVO
        https://github.com/MichaelGrupp/evo
        TUM format requires: timestamp x y z qx qy qz qw
        If poses is None, groundtruth is saved.
        if poses is already in [xyz, qx, qy, qz, qw] then use imu timestamp


        """
        tum_poses = []
        assert poses.shape[1] in [6, 7]
        output_directory = self.dataset_directory + 'tum_trajectories/'
        if poses is None or poses.shape[1] == 6:
            print("Saving groundtruth poses as TUM trajectory")
            poses = self.get_pose_from_gt()
            msgs = self.gts
            filename = output_directory + self.dataset_name + "_groundtruth_" + filename

        elif poses.shape[1] == 7:  # if poses are in xyz quat, is from imu
            print("Saving IMU orientations ")
            msgs = self.last_imus()
            filename = output_directory + self.dataset_name + "_imu_orientation_" + filename

        timestamps = [
            float("{}.{}".format(msg[b'header'][b'stamp'][b'secs'], msg[b'header'][b'stamp'][b'nsecs'])) for msg in
            msgs]
        tum_file = open(filename, 'w')
        for step in range(self.N):
            if poses.shape[1] == 6:
                qw, qx, qy, qz = euler_utils.euler2quat(z=poses[step, 5], y=poses[step, 4], x=poses[step, 3])
            elif poses.shape[1] == 7:
                qw, qx, qy, qz = poses[step, 3], poses[step, 4], poses[step, 5], poses[step, 6]

            tum_pose = "{} {} {} {} {} {} {} {}\n".format(timestamps[step], poses[step, 0], poses[step, 1],
                                                          poses[step, 2], qx, qy,
                                                          qz, qw)
            tum_poses.append(tum_pose)
            tum_file.write(tum_pose)
        tum_file.close()
        print("Written trajectory with tum format")

    def test_tf(self, step1, step2, shift_frame=True):
        # step1 = 44
        # step2 = 45
        pose1 = self.get_pose_from_gt(step1)
        pose2 = self.get_pose_from_gt(step2)
        pcd1 = self.pcds[step1]
        pcd2 = self.pcds[step2]

        red_color = [[255, 0, 0] for _ in pcd1]
        blue_color = [[0, 0, 255] for _ in pcd2]

        # matplotlib plot
        # ax = plt.axes(projection='3d')
        # ax.scatter(pcd1[:, 0], pcd1[:, 1], pcd1[:, 2], c="red")
        # ax.scatter(pcd2[:, 0], pcd2[:, 1], pcd2[:, 2], c="blue")
        # plt.show()

        pcd1_plt = make_open3d_point_cloud(pcd1, color=red_color)  # pcd at step 1
        pcd2_plt = make_open3d_point_cloud(pcd2, color=blue_color)  # pcd at step 2
        pcd1and2plt = make_open3d_point_cloud(np.vstack((pcd1, pcd2)),
                                              color=np.vstack((red_color, blue_color)))

        pose_diff = subtract_quat_pose(pose1, pose2)  # EXPRESSED IN ODOM!
        # pose_diff = np.matmul(self.get_tfmat_from_pose(pose1), np.linalg.inv(self.get_tfmat_from_pose(pose2)))
        # assemble transformation matrix of pose difference
        tf_mat = get_tfmat_from_pose(pose_diff)

        # transform pose difference to be relative movement of drone in drone frame
        if shift_frame:
            odom_frame_orientation = Quaternion(1, 0, 0, 0)
            pose1_mat = get_tfmat_from_pose(-pose1)
            # frame_shift = pose1_mat
            frame_shift_tf_mat = pose1_mat
            print("frame_shift:\n{}".format(frame_shift_tf_mat))
            pose_diff_bl_frame = np.matmul(tf_mat, invert_tfmat(frame_shift_tf_mat))
            # pose_diff_bl_frame = np.matmul(frame_shift_tf_mat, invert_tfmat(tf_mat))
            tf_mat = pose_diff_bl_frame

        print("\nsteps: {} {}\n\n=============================\n"
              "pose1: {}\n"
              "pose2: {}\n"
              "pose_diff: {}\n"
              "tf matrix: \n{}\n".format(step1, step2, pose1, pose2, pose_diff, tf_mat))

        # open3d transformation
        # transformed_pcd = pcd1_plt.transform(tf)  # pcd at step 1 transformed in step 2 frame

        # "manual" transformation
        ones = np.ones((len(pcd1), 1), int)
        to_transform = np.hstack((pcd1, ones))  # pad points with ones
        transformed_pcd = np.matmul(tf_mat, to_transform.T)[0:3, :]  # apply tf to padded points
        # rot90 = rot_z(-1.5450004)
        # transformed_pcd = np.matmul(rot90, transformed_pcd)
        green_color = [[0, 255, 0] for _ in pcd1]
        transformed_pcd_plt = make_open3d_point_cloud(transformed_pcd.T, green_color)

        # final_plt_pcd = make_open3d_point_cloud(np.vstack((pcd1and2plt.points, transformed_pcd_plt.points)),
        #                                         color=np.vstack((pcd1and2plt.colors, green_color)))

        # o3d.visualization.draw_geometries([final_plt_pcd], window_name="DeepLoc")
        # o3d.visualization.draw_geometries([pcd1_plt, pcd2_plt, transformed_pcd_plt], window_name="DeepLoc")

        vis = o3d.visualization.Visualizer()
        vis.create_window(window_name="test tf")
        vis.get_render_option().show_coordinate_frame = True
        vis.add_geometry(pcd1_plt)
        vis.add_geometry(pcd2_plt)
        vis.add_geometry(transformed_pcd_plt)
        vis.run()
        vis.destroy_window()
        # matplotlib plot
        # ax.scatter(transformed_pcd[:, 0], transformed_pcd[:, 1], transformed_pcd[:, 2], c="green")
        # plt.show()

    def test_tf_homogeneous_mat(self, step1, step2, plot='o3d', shift_frame=False):
        pose1 = self.get_pose_from_gt(step1)
        pose2 = self.get_pose_from_gt(step2)
        pcd1 = self.pcds[step1]
        pcd2 = self.pcds[step2]

        red_color = [[255, 0, 0] for _ in pcd1]
        blue_color = [[0, 0, 255] for _ in pcd2]

        if plot == 'o3d':
            pcd1_plt = make_open3d_point_cloud(pcd1, color=red_color)  # pcd at step 1
            pcd2_plt = make_open3d_point_cloud(pcd2, color=blue_color)  # pcd at step 2
            pcd1and2plt = make_open3d_point_cloud(np.vstack((pcd1, pcd2)),
                                                  color=np.vstack((red_color, blue_color)))
        else:
            # matplotlib plot
            ax = plt.axes(projection='3d')
            ax.scatter(pcd1[:, 0], pcd1[:, 1], pcd1[:, 2], c="red")
            ax.scatter(pcd2[:, 0], pcd2[:, 1], pcd2[:, 2], c="blue")

        pose1_mat = get_tfmat_from_pose(pose1)
        pose2_mat = get_tfmat_from_pose(pose2)
        # pose_diff is pose1 - pose2
        # pose_diff_mat = np.matmul(pose2_mat, invert_tfmat(pose1_mat))
        pose_diff_mat = np.matmul(pose1_mat, np.linalg.inv(pose2_mat))


        # transform pose difference to be relative movement of drone in drone frame
        if shift_frame:
            frame_shift = invert_tfmat(pose1_mat)
            # odom_frame = get_tfmat_from_pose(np.array([0, 0, 0, 1, 0, 0, 0]))
            # frame_shift = invert_tfmat(odom_frame)
            print("frame_shift:\n{}".format(frame_shift))

            # pose_diff_bl_frame = np.matmul(pose_diff_mat, frame_shift)
            pose_diff_bl_frame = np.matmul(frame_shift, pose_diff_mat)
            pose_diff_mat = pose_diff_bl_frame
        print("\n{} {}\n\n=============================\n"
              "pose1: \n{}\n\n"
              "pose2: \n{}\n\n"
              "pose_diff: \n{}\n\n".format(step1, step2, pose1_mat, pose2_mat, pose_diff_mat))

        # open3d transformation
        # transformed_pcd = pcd1_plt.transform(pose_diff_mat)  # pcd at step 1 transformed in step 2 frame

        # "manual" transformation
        ones = np.ones((len(pcd1), 1), int)
        to_transform = np.hstack((pcd1, ones))
        transformed_pcd = np.matmul(to_transform, pose_diff_mat.T)[:, 0:3]  # this one

        # transformed_pcd = np.matmul(pose_diff_mat, to_transform.T)[:3, :]
        transformed_pcd = transformed_pcd.T

        if plot == 'o3d':
            green_color = [[0, 255, 0] for _ in pcd1]
            transformed_pcd_plt = make_open3d_point_cloud(transformed_pcd, green_color)
            # final_plt_pcd = make_open3d_point_cloud(np.vstack((pcd1and2plt.points, transformed_pcd_plt.points)),
            #                                         color=np.vstack((pcd1and2plt.colors, green_color)))
            # o3d.visualization.draw_geometries([final_plt_pcd], window_name="DeepLoc")
            # o3d.visualization.draw_geometries([pcd1_plt, pcd2_plt, transformed_pcd_plt], window_name="DeepLoc")
            vis = o3d.visualization.Visualizer()
            vis.create_window(window_name='test tf homogeneous')
            vis.get_render_option().show_coordinate_frame = True
            vis.add_geometry(pcd1_plt)
            vis.add_geometry(pcd2_plt)
            vis.add_geometry(transformed_pcd_plt)
            vis.run()
            vis.destroy_window()
        else:
            # matplotlib plot
            ax.scatter(transformed_pcd[:, 0], transformed_pcd[:, 1], transformed_pcd[:, 2], c="green")
            plt.show()
