import pydevd_pycharm
import numpy as np
from data_preprocess import DataPreprocessor
import utils
import open3d as o3d
import time


# pydevd_pycharm.settrace('localhost', port=9000, stdoutToServer=True, stderrToServer=True)

def plot_with_evo():
    root = "/home/alescop/Documents/THESIS/src/DeepLocalization/"
    imu_tum = root + "tum_poses.txt"
    gt_tum = root + "dataset_tum.txt"
    # evo_plot_imuvsgt_trajectories(imu_tum, gt_tum)


def debug_rpy():
    imu_rpy = dp.get_rpy_from_imu()
    gt_rpy = dp.get_rpy_from_gt()

    difference = gt_rpy - imu_rpy

    for step in range(dp.N):
        print("\n\n====================== STEP {}\nIMU: {}\nGT: {}\nDIFFERENCE: {}".format(step, imu_rpy[step],
                                                                                           gt_rpy[step],
                                                                                           difference[step]))


def move_camera(vis):
    ctr = vis.get_view_control()
    global idx
    to_plot = utils.make_open3d_point_cloud(dp.pcds[idx])
    idx = idx + 1 if idx < 45 else idx
    vis.add_geometry(to_plot)
    ctr.rotate(0.0, 5.0)
    vis.poll_events()
    vis.update_renderer()
    time.sleep(1)


def plot_pcds_onebyone():
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    # vis.register_animation_callback(move_camera)
    first = utils.make_open3d_point_cloud(dp.pcds[0])
    vis.add_geometry(first)
    # vis.run()
    for pcd in dp.pcds[1:]:
        # to_plot = utils.make_open3d_point_cloud(pcd)
        # vis.add_geometry(to_plot)
        first = utils.make_open3d_point_cloud(np.vstack((first.points, pcd)))
        vis.poll_events()
        vis.update_geometry(first)
        vis.update_renderer()
        time.sleep(1)
    ctr = vis.get_view_control()
    ctr.rotate(0.0, 5.0)
    # time.sleep(120)
    cmd = input("close?")
    while cmd != 'yes':
        cmd = input("close?")
    vis.destroy_window()


def test_all_tf(homogeneous, jump=1, shift_frame=False):
    for i in range(jump, dp.N, jump):
        if homogeneous == False:
            dp.test_tf(i - jump, i, shift_frame=shift_frame)
        elif homogeneous == True:
            dp.test_tf_homogeneous_mat(i - jump, i, shift_frame=shift_frame)
        elif homogeneous == "both":
            dp.test_tf(i - jump, i, shift_frame=shift_frame)
            dp.test_tf_homogeneous_mat(i - jump, i, shift_frame=shift_frame)


if __name__ == "__main__":
    # for npz
    use_bag = True
    dataset_directory = '/media/alescop/Data/mainblades_data/deeploc_bags/'
    filename = ""

    # filename = 'test4_going_up_m100_2020-07-14-10-44-30.npy'
    # filename = 'test9_takoff_left_base_linkframe_notf_2020-07-16-11-48-46.npy' # working properly but with shit pcd
    # filename = 'test10_takoff_left_base_linkframeassembler_notf_2020-07-16-12-09-03.npy' # works properly, shit pcd pose1 - pose2
    # filename = 'test11_takoff_left_right_with_tf_2020-07-16-12-47-30.npy' # using tf in ros, alignment doesn't happen
    # filename = 'test12_takoff_front_back_notfusingbaselinkframe_2020-07-16-14-11-01.npy'
    # in test 12 alignment happens for consecutive steps, for 5 steps difference it is slightly wrong.
    # step 44-45 show double fuselage, probably because of base_link frame in assembler?

    # filename = 'test13_backtoodom_usingtf_2020-07-20-18-12-11.npy'
    # filename = 'final_test_newimucheck_2020-07-22-11-20-38.npy'
    # pcds still assembled in base_link frame, okish but with big step difference no alignment

    # filename = 'imu_check_odom_frame_invertedtf_2020-07-22-12-20-55.npy'
    # filename = 'pcd_check_odom-assembled_BL-OD-tf_2020-07-23-12-47-41.npy'  # WORKS, 39-44 shows some mis-alignment
    # filename = 'pcd_check_odom-assembled_OD-BL-tf_2020-07-23-14-09-30.npy'  # 5-10 work(ish) 30-40 is completely wrong
    # filename = 'pcd_check_odom-assembled_BL-OD-tf_goingleft_2020-07-24-11-29-52.npy'
    # 10-15 very nice, 30-35/35-40 very bad because of yaw
    # filename = 'pcd_check_odom-assembled_OD-BL-tf_goingleft_2020-07-24-11-54-47.npy'
    # with aggressive roll the alignment fucks up, from 10 on
    # filename = 'general_checks_going_slow_2020-07-24-14-15-31.npy'
    # checked all tf, when looking behind there is misalignment
    # filename = 'look_back_full_check_mid_speed_2020-07-24-14-47-19.npy'
    # shows misalignment when looking back
    # filename = 'check_tf_goingright_2020-07-24-16-33-05.npy'
    # filename = 'check_tf_yawright90_2020-07-24-16-40-10.npy'

    # filename = 'check_tf_llokingback_assembledBL_2020-07-27-13-52-39.npy'
    # filename = 'check_tf_lookingback_assembledBL_2020-07-27-13-57-55.npy'
    # filename = 'check_tf_lookingback_assembledBL_2020-07-27-14-02-28.npy'
    # filename = 'check_tf_lookingback_assembledBL_2020-07-27-14-20-26.npy'
    # filename = 'remcothe_savior_2020-07-27-17-13-00.npy'
    # filename = 'remcothe_savior2_2020-07-27-18-04-23.npy'
    # filename = 'scan_to_cloud_bl_lookbacktocheck_2020-07-29-19-32-35.npy'

    # basic movements
    # filename = 'fixyaw_frontleftbackright_2020-07-30-12-00-37.npy'
    # filename = 'fixyaw_backright_2020-07-30-12-16-26.npy'
    # filename = 'yawr90_frontleftbackright_2020-07-30-14-22-52.npy'
    # filename = 'MPC_yawL90_frontleftbackright_yawr90_2020-07-30-14-42-31.npy'
    # filename = 'base_link_notf_2020-07-30-18-17-44.npy'
    # filename = 'base_link_notf_2020-07-30-18-21-12.npy'
    # filename = 'test3_flbr_2020-07-31-14-30-44.npy'
    # test3: test tf works always, test tf homog almost works, but sometimes there is inconsistency in z

    filename = 'test4_y90flbr_2020-07-31-14-36-41.npy'
    # filename = 'test4_y90flbrym90flbr_2020-07-31-14-58-30.npy'
    # filename = 'ouster_test_2020-07-31-15-58-47.npy'
    # filename = 'base_link_pose_wrtprevstep_2020-08-03-18-00-11.npy'

    config_file = None
    dp = DataPreprocessor(dataset_directory, filename, config_file)

    # Set printing precision
    np.set_printoptions(precision=3, suppress=True)

    # debug_rpy()
    # dp.get_xyz_from_gt()
    # dp.absolute_to_relative_pose()
    # dp.get_quat_from_imu()
    dp.plot_pcds()
    # plot_pcds_onebyone()
    # dp.plot_all_imus()
    # dp.plot_trajectory()
    # dp.save_as_tum()
    # dp.test_tf(1, 5)
    # dp.test_tf_homogeneous_mat(1, 5)
    # test_all_tf(homogeneous="both", jump=3)
    test_all_tf(homogeneous=False, jump=3, shift_frame=True)
    # test_all_tf(homogeneous=True, jump=3)
