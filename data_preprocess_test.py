from data_preprocess import *
import unittest


class TestDataPreprocess(unittest.TestCase):
    def setUp(self) -> None:
        # for npz
        use_bag = False
        dataset_directory = '/home/alescop/Documents/THESIS/src/DeepLocalization/data/'
        filename = 'test_n74.npz'
        self.dp = DataPreprocessor(dataset_directory, filename, use_bag)

    def test_subtract_pose(self):
        # TODO: test subtract pose
        pass

    def test_add_pose(self):
        # TODO: test add pose
        pass

    def test_get_rpy_from_imus(self):
        self.dp.plot_all_imus()

    def test_tf(self):
        step1 = 10
        step2 = 18
        pose1 = self.dp.get_pose_from_gt(step1)
        pose2 = self.dp.get_pose_from_gt(step2)
        pcd1 = self.dp.pcds[step1]
        pcd2 = self.dp.pcds[step2]
        red_color = [[255, 0, 0] for _ in pcd1]
        blue_color = [[0, 0, 255] for _ in pcd2]
        pcd1_plt = make_open3d_point_cloud(pcd1, color=red_color)
        pcd2_plt = make_open3d_point_cloud(pcd2, color=blue_color)
        pcd1and2plt = make_open3d_point_cloud(np.vstack((pcd1, pcd2)),
                                              color=np.vstack((red_color, blue_color)))
        pose_diff = subtract_pose(pose2, pose1)
        print("\n\n\n=============================\n"
              "pose1: {}\n"
              "pose2: {}\n"
              "pose_diff: {}\n".format(pose1, pose2, pose_diff))

        # ======================= O3D =======================
        #
        # vis = o3d.visualization.Visualizer()
        # vis.create_window(window_name="DeepLoc")
        # vis.add_geometry(pcd1and2plt)
        # vis.poll_events()
        # vis.update_renderer()
        # vis.destroy_window()

        tf = euler_utils.pose_vec_to_mat(pose_diff)
        transformed_pcd = pcd1_plt.transform(tf)
        green_color = [[0, 255, 0] for _ in pcd1_plt.points]
        final_plt_pcd = make_open3d_point_cloud(np.vstack((pcd1and2plt.points, transformed_pcd.points)),
                                                color=np.vstack((pcd1and2plt.colors, green_color)))

        # # generate transformed pcd
        # new_pcd = make_open3d_point_cloud(pcd1)
        # tf_pcd = new_pcd.transform(tf)
        # green_color = [[0, 255, 0] for _ in tf_pcd.points]
        # tf_pcd.colors = o3d.utility.Vector3dVector(green_color)

        o3d.visualization.draw_geometries([pcd1and2plt], window_name="DeepLoc")


if __name__ == '__main__':
    unittest.main()
    # dp.save_as_tum()
    # dp.plot_all_pcds()
    # dp.plot_all_imus()
    # dp.absolute_to_relative_pose()
    # dp.plot_trajectory()
    # dp.test_tf()
